package work.home.weatherapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextClock;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.Objects;

import work.home.weatherapp.adapter.WeatherResultAdapter;
import work.home.weatherapp.models.entities.WeatherResult;
import work.home.weatherapp.presenters.WeatherPresenter;

public class MainActivity extends AppCompatActivity implements MainView {

    private static final int PERMISSION_REQUEST = 101001011;

    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private WeatherResultAdapter adapter;
    private RecyclerView recyclerView;
    private WeatherPresenter presenter;
    private WeatherResult weatherResult;

    private TextClock clock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );

        setContentView(R.layout.activity_main);

        clock = findViewById(R.id.right_side_time_txt_id);
        Typeface font = Typeface.createFromAsset(
                getAssets(),
                "fonts/helvetica_neue_lt_std_thcn.otf");
        clock.setTypeface(font);

        recyclerView = findViewById(R.id.recyclerView_id);
        if (recyclerView != null) {
            recyclerView.setHasFixedSize(true);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        Objects.requireNonNull(recyclerView).setLayoutManager(layoutManager);

        presenter = new WeatherPresenter(this);

        requestLocation();
    }

    private void requestLocation() {
        buildLocationRequest();
        buildLocationCallback();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                        new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                        },
                        PERMISSION_REQUEST
                );
            }
        }

        FusedLocationProviderClient fusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(this);
        fusedLocationProviderClient
                .requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    private void buildLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setSmallestDisplacement(100.0f);
    }

    private void buildLocationCallback() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location l : locationResult.getLocations()) {
                    presenter.loadWeather(l);
                }
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_REQUEST) {
            if (grantResults.length == 2
                    && (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    || grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                requestLocation();
            }
        }
    }

    @Override
    public void updateRecyclerView(WeatherResult result) {
        this.weatherResult = result;
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean checkForNull() {
        return adapter == null;
    }

    @Override
    public void initRecyclerView(WeatherResult weatherResult) {
        adapter = new WeatherResultAdapter(weatherResult);
        recyclerView.setAdapter(adapter);
    }
}
