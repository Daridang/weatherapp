package work.home.weatherapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import work.home.weatherapp.R;
import work.home.weatherapp.models.entities.DateList;
import work.home.weatherapp.models.entities.WeatherResult;
import work.home.weatherapp.presenters.WeatherPresenter;

/**
 * Created by
 * +-+-+-+-+-+-+-+-+
 * |D|a|r|i|d|a|n|g|
 * +-+-+-+-+-+-+-+-+
 * on 2019-05-26.
 */
public class WeatherResultAdapter extends
        RecyclerView.Adapter<WeatherResultAdapter.WeatherViewHolder> {

    private WeatherResult weatherResults;

    public WeatherResultAdapter(WeatherResult weatherResults) {
        this.weatherResults = weatherResults;
    }

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new WeatherViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.weather_list_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder holder, int position) {
        DateList result = weatherResults.getList().get(position);
        holder.temperature.setText(String.valueOf(result.getMain().getTemp()));
        holder.city.setText(weatherResults.getCity().getName());
        holder.date.setText(result.getDtTxt());
        holder.image.setImageResource(R.drawable.weather_icon_demo);
    }

    @Override
    public int getItemCount() {
        return weatherResults.getList().size();
    }

    public static class WeatherViewHolder extends RecyclerView.ViewHolder {

        public TextView temperature;
        public TextView city;
        public TextView dayOfWeek;
        public TextView date;
        public ImageView image;

        public WeatherViewHolder(@NonNull View itemView) {
            super(itemView);
            temperature = itemView.findViewById(R.id.temperature_txt_id);
            city = itemView.findViewById(R.id.city_name_txt_id);
            dayOfWeek = itemView.findViewById(R.id.day_of_week_txt_id);
            date = itemView.findViewById(R.id.date_txt_id);
            image = itemView.findViewById(R.id.weather_icon_img_id);
        }
    }
}
