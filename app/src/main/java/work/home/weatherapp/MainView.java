package work.home.weatherapp;

import work.home.weatherapp.models.entities.WeatherResult;

/**
 * Created by
 * +-+-+-+-+-+-+-+-+
 * |D|a|r|i|d|a|n|g|
 * +-+-+-+-+-+-+-+-+
 * on 2019-06-03.
 */
public interface MainView {

    void updateRecyclerView(WeatherResult result);
    boolean checkForNull();
    void initRecyclerView(WeatherResult result);
}
