package work.home.weatherapp.models.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by
 * +-+-+-+-+-+-+-+-+
 * |D|a|r|i|d|a|n|g|
 * +-+-+-+-+-+-+-+-+
 * on 03/01/2019.
 */
@NoArgsConstructor
@Getter
@Setter
public class Sys {
    private float type;
    private float id;
    private float message;
    private String country;
    private float sunrise;
    private float sunset;
    @SerializedName("pod")
    @Expose
    private String pod;
}