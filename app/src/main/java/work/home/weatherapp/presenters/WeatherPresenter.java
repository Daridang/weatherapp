package work.home.weatherapp.presenters;

import android.location.Location;
import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import work.home.weatherapp.MainView;
import work.home.weatherapp.models.entities.WeatherResult;
import work.home.weatherapp.models.retrofit.IOpenWeatherMap;
import work.home.weatherapp.models.retrofit.RetrofitClient;

/**
 * Created by
 * +-+-+-+-+-+-+-+-+
 * |D|a|r|i|d|a|n|g|
 * +-+-+-+-+-+-+-+-+
 * on 2019-06-03.
 */
public class WeatherPresenter {

    private IOpenWeatherMap iOpenWeatherMap;
    private Disposable disposable;
    private MainView mainView;

    public WeatherPresenter(MainView mainView) {
        this.mainView = mainView;
    }

    public void loadWeather(Location location) {
        iOpenWeatherMap = RetrofitClient.getInstance().create(IOpenWeatherMap.class);
        Observable<WeatherResult> resultObservable = iOpenWeatherMap.getWeatherByCoordinates(
                IOpenWeatherMap.API_KEY,
                String.valueOf(location.getLatitude()),
                String.valueOf(location.getLongitude()),
                "metric"
        ).subscribeOn(Schedulers.io());

        disposable = resultObservable.observeOn(AndroidSchedulers.mainThread()).subscribe(
                weather -> {
                    if (mainView.checkForNull()) {
                        mainView.initRecyclerView(weather);
                    }
                    mainView.updateRecyclerView(weather);

                },
                throwable -> {
                    Log.d("TAGG", throwable.getMessage());
                }
        );
    }

    public void dispose() {
        disposable.dispose();
    }
}
